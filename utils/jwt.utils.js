let jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = '3fsufkshekjfefs5876fsevjhsef564jgkgyjvgs55dfgse4f2s5e4fd5qzregfv8';

module.exports = {
    generateTokenforUser : function (userData) {
        return jwt.sign({
                userID : userData.id,
            },
            JWT_SIGN_SECRET,
            {
                expiresIn: '1h'
            })
    },
    parseauthorisation : function (authorization) {
        return (authorization != null) ? authorization.replace('Bearer ','') : null ;
    },
    /**
     * @return {number}
     */
    GetUserId: function (authorization) {
        let  Userid = -1 ;
        let token = module.exports.parseauthorisation(authorization);
        if (token != null) {
            try {
                let jwtToken = jwt.verify(token,JWT_SIGN_SECRET);
                if (jwtToken != null)
                    Userid = jwtToken.userID ;
            } catch (e) { }
        }
        return Userid;
    }
};