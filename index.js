let express = require('express');
let boarder = require('body-parser');
let apiRoute = require('./apiRouter').router;

let server = express();

server.use(boarder.urlencoded({ extended: true }));
server.use(boarder.json());


server.use('/api/',apiRoute);

server.listen('8080',function (){
    console.log('Serveur OK') ;
});