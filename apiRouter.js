let express = require('express');

let userCtrl = require('./control/user');


exports.router = (function (){
    let apiRouter = express.Router();
    //user
    apiRouter.route('/users/register/').post(userCtrl.register);
    apiRouter.route('/users/login/').post(userCtrl.login);
    apiRouter.route('/users/delete/').delete(userCtrl.delete);
    apiRouter.route('/users/info/').get(userCtrl.getUserProfile);
    apiRouter.route('/users/info/').put(userCtrl.updateUserProfile);
    
});