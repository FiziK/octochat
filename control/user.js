let models = require('../models');
let uuidv4 = require('uuid/v4');
let bcrypt = require('bcrypt');
let jwtutil = require('../utils/jwt.utils');

module.exports = {
    register : function (req,res){
        let email = req.body.email;
        let nom = req.body.nom;
        let prenom = req.body.prenom;
        var password = req.body.password;
        let tel = req.body.tel;
        let type = req.body.type;

        if (email == null || nom == null || prenom == null || password == null || tel == null || type == null){
            return res.status(400).json({'error' : 'missing parameters'});
        }
        else{
            bcrypt.hash(password,5,function (err,bcryptpassword) {
            models.User.create({
                id : uuidv4(),
                Nom : nom ,
                Prenom : prenom ,
                email : email ,
                password : bcryptpassword,
                tel : tel,
                type : type
            })
            .then((nouvelUser) => {
                return res.status(201).json({'userID': nouvelUser.id})
            })
            .catch((err) => {
                // erreur lors de la creation de l'utilisateur 7
                return res.status(500).json({'error' : 'cannot create user'});
            })
        })
      }
    },
    login : function (req,res) {
        let email = req.body.email;
        let password = req.body.password;

        models.User.findOne({
            where : {email : email}
        })
            .then((userlogin) => {
                if (userlogin){
                    bcrypt.compare(password, userlogin.password, function (errcrypt,rescrypt) {
                        if (rescrypt){
                            return res.status(200).json({
                                'userID': userFound.id,
                                'pseudo' : userFound.pseudo,
                                'token' : jwtutil.generateTokenforUser(userlogin)
                            })
                        }
                    })
                }
                else {
                    return res.status(404).json({'error' : 'user not exist in DB'});
                }
            })
            .catch((errlogin) => {
                return res.status(500).json({'error' : 'unable to verify user'});
            })
    }
}