'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    Nom: DataTypes.STRING,
    Prenom: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    tel: DataTypes.STRING,
    type: DataTypes.ENUM({
      values: ['Parent', 'Admin','Etudiant']
    })
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};